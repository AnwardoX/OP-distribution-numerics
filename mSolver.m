BeginPackage["OPDistribution`mSolver`"];

setModelParameters::usage = 
    "setModelParameters[k, nu0, v] sets the value of model constants kappa, nu0, v appropriately.
    setModelParameters[k, nu0, v, useCorr] additionally sets the value of the 'useCorrections' flag.
    If the update is successfull, the state of the approximation is reset back to 'not solved'.
    The function retunrs True on success. Otherwise, returns False or remains unevaluated.";
setSolverParameters::usage = 
    "setSolverParameters[R, precision, maxNodes] sets the value of internal parameters of the method.
    Each supplied argument either match the setting type or have the  'Automatic' value, in which case it remains unchanged.
    The types of the arguments are: Positive Real, Positive Real, Positive Real.
    If the update is successfull, the state of the approximation is reset back to 'not solved'.
    The function retunrs True on success. Otherwise, returns unevaluated.";
updateSolution::usage = 
    "updateSolution[] performs the procedure of solving the integral equation on r1 with the specified parameters.
    If the update was performed and parameters were not changed, a new update is not performed.
    Rerurns the list {len, err}, where len is the number of w-nodes used, and err the error estimation.";
sigmaNIntegrate::usage = 
    "sigmaNIntegrate[func, R, prec] integrates the Pure function func over the standard s contour
    with radius R and precision prec.
    sigmaNIntegrate[func, R, prec, cutoff] does the same as sigmaNIntegrate[func, R, prec], but 
    cuts the integration region on Abs[s] = cutoff."
naturalCutoff::usage = 
    "naturalCutoff[u0, lambda] returns the theoretical estimation on a sufficient value of the cut off 
    for the sigma integral.";

collectSolutionState::usage = 
    "collectApproximationState returns the current structure of the solution suitable for saving and loading.
    Accepts a single mandatory Boolean argument that controls whether cached values of various functions are also to be saved.";
exportSolutionState::usage = 
    "exportApproximationState[fileName, saveStorage] collects the current state of the solution by
    collectSolutionState[saveStorage] and saves it to a binary file 'fileName'.
    Returns True on sucess. Otherwise, returns False or remains unevaluated.";
validSolutionStateQ::usage = 
    "validSolutionStateQ[state] tests whether the argument is a valid structure of the Solution State.";
loadSolutionState::usage = 
    "loadApproximationState[state] replaces the current state of the solution with the one provided.
    The approximation is reset, so that all cached values of special functions are lost.
    Returns True on success, otherwise returns unevaluated.";
importSolutionState::usage = 
    "importSolutionState[fileName] replaces the current state of the solution with the one stored in the file 'fileName'.
    The approximation is reset, so that all cached values of special functions are lost.
    Returns True on sucess. Otherwise, returns False or remains unevaluated.";

Begin["`Private`"];

    Needs["OPDistribution`Common`", "Common.m"];
    Needs["OPDistribution`mFunction`", "mFunction.m"];

    setModelParameters[uN_?positiveRealQ, l_?positiveRealQ, useCorr_?BooleanQ] :=
        If[
            setApproximationParameters[uN, l, useCorr] === True,
            {u0, lambda, useCorrections} = {uN, l, useCorr};
            solutionState["is-solved"] = False;
            True,
            messagePrint[
                "WRN",
                "setModelParameters:",
                "mFunction`setApproximationParameters call failed. Arguments: ",
                {uN, l, useCorr}
            ];
            False
        ];
    setModelParameters[uN_?positiveRealQ, l_?positiveRealQ] :=
        setModelParameters[uN, l, useCorrections];
    setSolverParameters[
        R_?positiveRealQ|R:Automatic, 
        prec_?positiveRealQ|prec:Automatic, 
        maxNodes_?positiveIntegerQ|maxNodes:Automatic
        ] := 
        (
            If[
                !MatchQ[
                    {R, prec, maxNodes},
                    {contourRadius|Automatic, targetPrecision|Automatic, maxNodeCount|Automatic}
                ],
                {contourRadius, targetPrecision, maxNodeCount} = 
                    {
                        R/.Automatic->contourRadius,
                        prec/.Automatic->targetPrecision,
                        maxNodes/.Automatic->maxNodeCount
                    };
                solutionState["is-solved"] = False
            ];
            True
        );

    (*init*)
    (*a set of characteristics of the current approximation;
    should be initialized with something BEFORE the 1st call of
    setModelParameters or setSolverParameters
    because the latter modifies the value of solutionState*)
    solutionState = 
        Association[
            "is-solved" -> False,
            "reference-values" -> {1}, 
            "extra-node-count" -> 0, (*Always equal to Length@"reference-values" - 1*)
            "error" -> Infinity,
            "new-node-count" -> 0
        ];
    setModelParameters[0.2, 0.2, True]; (*here we use the full version should always be called,
    because it sets the useCorrection variable*)
    setSolverParameters[0.1, 5, 14];
    (*maximum number of nodes is 14, because beyond that the restoration procedure 
    fails when solving the linear equation, as suggested by numerical experiments.*)

    sigmaNIntegrate[
        func_Function, R_?positiveRealQ, precision_?positiveRealQ,
        Optional[cutoff_?positiveRealQ, Infinity]
    ] /; NumericQ[func[R]] && cutoff > R :=
        Block[
            {val, error},
            val = NIntegrate[
                func[R Exp[-I (Pi - x)]] * (I R Exp[-I (Pi - x)]),
                {x, 0, Pi}, AccuracyGoal -> precision
            ] + NIntegrate[
                func[s] + func[-s],
                {s, R, cutoff}, AccuracyGoal -> precision
            ];
            If[
                cutoff < Infinity,
                error = N@Abs[func[cutoff]];
                messagePrint[
                    "DBG", "sigmaNIntegrate:",
                    "Precision bound from a finite cut-off: ",
                    - Log[10, error]
                ]
            ];
            val
        ];
    SetAttributes[sigmaNIntegrate, HoldAll];
    (*the integral converges in a finite region of sigma; 
    there's a good analytical esimation on the size of this region:*)
    naturalCutoff[u0_?positiveRealQ, l_?positiveRealQ] :=
        (80 / Pi) (1 / l) Max[1, ProductLog[(Pi/4) u0] / (Pi/4)];

    w0EquationExact[] :=
        Block[
            {w0Val = m1[0] / u0},
            w0Val * Log[1/u0] + Pi/4 + j[0]
            + Re@sigmaNIntegrate[
                (Exp[m[#, 0]] phi12[#])&,
                contourRadius, targetPrecision + 1,
                naturalCutoff[u0, lambda]
            ] / (2 Pi)
        ];
    w0EquationApproximate[] :=
        Block[
            {w0Val = m1[0] / u0},
            w0Val * Log[1/u0] + Pi/4 + w0Val Log[1/w0Val]
             + lambda (Pi/4 Log[1/u0] + Re@f[w0Val])
        ];
    m1EquationExact[w_?validWQ] := 
        (
            (m1[w] - m1[0]) / u0 - w - lambda * 
            (
                w Log[1/u0]
                + Re@sigmaNIntegrate[
                    (Exp[m[#, 0]] (Exp[I # w] - 1) phi12[#])&,
                    contourRadius, targetPrecision + 1,
                    naturalCutoff[u0, lambda]
                ] / (2 Pi)
            )
        );(*automatically satsified for w = 0, so w0Equation should be used instead*)

    validValuesQ = 
        Function[
            {x}, 
            positiveRealVectorQ[x] 
            && Length[x] >= 1
        ];
    equations[values_?validValuesQ] :=
        (
            updateReferenceValues[First@values, Rest@values];
            If[
                useCorrections,
                {w0EquationExact[]} ~Join~
                Thread[m1EquationExact[getExtraNodes[]]],
                {w0EquationApproximate[]}
            ]
        );

    m1ValuesInitialApproximation[nc_?nonnegativeIntegerQ] :=
        Block[
            {w0Val, nds},
            nds = {0} ~Join~ getExtraNodes[nc];
            w0Val = (Pi/4)/ProductLog[(Pi/4) u0];
            Table[
                (*this is a cheat to avoid typos;
                works because the values of u0, lambda are consistent*)
                OPDistribution`mFunction`Private`m1LeadingPure[w0Val, w]
                + OPDistribution`mFunction`Private`m1SingularPure[w0Val, w],
                {w, nds}
            ]
        ];

    optimizeValues[initApprox_?validValuesQ] :=
        Block[
            {sol},
            sol = FindRoot[
                equations[values],
                {   
                    (*the initial approximation defines the number of nodes to use*)
                    values, initApprox,
                    ConstantArray[N[10^(-15)], Length@initApprox],
                    ConstantArray[Infinity, Length@initApprox]
                },
                StepMonitor :> messagePrint[
                    "DBG", "optimizeValues:",
                    "current values: ", values,
                    "current error: ", equations[values]
                ],
                EvaluationMonitor :> messagePrint[
                    "DBG", "optimizeValues:",
                    "evaluating at: ", values
                ],
                AccuracyGoal -> targetPrecision / 2
            ];
            If[
                MatchQ[sol, {values -> _?validValuesQ}],
                {True, values/.sol[[1]]},
                (*some error occured*)
                messagePrint[
                    "ERR", "optimizeValues:", 
                    "FindRoot call failed. The most recent successfull result: ",
                    Thread[m1[{0} ~Join~ getExtraNodes[]]]
                ];
                {False, Thread[m1[{0} ~Join~ getExtraNodes[]]]}
            ]
        ];
        (*returns the status of the optimization and the list of reference values*)
    
    (*estimate current accuracy*)
    currentErrorEstimate[extraNodeCount_?nonnegativeIntegerQ] := 
        Block[
            {nds},
            nds = getExtraNodes[extraNodeCount + 3];(*get a denser list of nodes*)
            u0 * Norm[Thread[m1EquationExact@nds]] / Length@nds
            (*note that m1EquationExact call does not lead to the reset of the approximation
            using equations[] call instead would call a reset of memorized values*)
        ];
    newNodeCount[extraNodeCount_?nonnegativeIntegerQ, currentError_?nonnegativeRealQ] := 
        Block[
            {n},
            (*based on the empirical formula for the error:
            eps = 10^(- (n + 3) / 2), where n is number of nodes*)
            n = extraNodeCount + 2 (targetPrecision + Log[10, currentError]);
            Which[
                n < extraNodeCount || extraNodeCount == maxNodeCount,
                extraNodeCount + 1,
                n < maxNodeCount, Ceiling[n],
                True, maxNodeCount
            ]
        ];
    updateSolutionState[initialApproximation_?validValuesQ] :=
        (
            {
                solutionState["is-solved"],
                solutionState["reference-values"]
            } = optimizeValues[initialApproximation];
            solutionState["error"] = currentErrorEstimate[solutionState["extra-node-count"]];
            solutionState["new-node-count"] = 
                newNodeCount[
                    solutionState["extra-node-count"],
                    solutionState["error"]
                ];
            True
        );

    buildApproximation[] :=
        Block[
            {initApprox},
            If[
                useCorrections,
                solutionState["extra-node-count"] = 1,
                solutionState["extra-node-count"] = 0
            ];
            (*build initial approximation*)
            initApprox = m1ValuesInitialApproximation[solutionState["extra-node-count"]];
            messagePrint[
                    "DBG","buildApproximation:",
                    "Initial approximation: ", initApprox
                ];
            updateSolutionState[initApprox];
            (*if corrections are not used, this already determines w0 correctly*)
            (*the loops below keeps adding more nodes until success;
            if useCorrections == False, then this part is just skipped*)
            While[
                (
                    useCorrections
                    && solutionState["is-solved"] === True
                    && solutionState["error"] >= 10^(-targetPrecision)
                    && solutionState["new-node-count"] <= maxNodeCount
                ),
                messagePrint[
                        "INF","buildApproximation:",
                        "Increasing number of nodes.", "Current value of error: ", solutionState["error"],
                        "New number of nodes: ", solutionState["new-node-count"]
                    ];
                (*increase number of nodes**)
                solutionState["extra-node-count"] = solutionState["new-node-count"];
                (*using the current approximation
                to obtain initial values for the next iteration*)
                initApprox = 
                    Thread[
                        m1[{0} ~Join~ getExtraNodes[solutionState["extra-node-count"]] ]
                    ];
                updateSolutionState[initApprox];
            ]; 
            (*analyze the exit state*)
            Which[
                (*more strict check here because optimizeValues could have returned rubbish*)
                solutionState["is-solved"] =!= True,
                solutionState["is-solved"] = False;
                messagePrint[
                    "ERR", "buildApproximation:",
                    "optimizeValues call failed."
                ],

                solutionState["new-node-count"] >= maxNodeCount,
                messagePrint[
                    "WRN", "buildApproximation:",
                    "The maximum number of nodes was reached. ",
                    "Requested number of nodes: ", solutionState["new-node-count"]
                ]
            ];
            (*return the final state*)
            solutionState
        ];

    updateSolution[] := 
        If[
            !solutionState[[1]], 
            buildApproximation[], 
            solutionState
        ];

    collectSolutionState[saveStorage_] := 
        Block[
            {apprState},
            apprState = collectApproximationState[saveStorage];
            Association[
                "solution-state" -> solutionState,
                "approximation-state" -> apprState
            ]
        ];
    exportSolutionState[fileName_String, saveStorage_?BooleanQ] :=
        exportObject[collectSolutionState[saveStorage], fileName];
    
    validSolutionStateStructureQ = 
        Function[
            {x},
            AssociationQ[x] && 
            And@@Map[
                KeyExistsQ[x, #]&,
                {"is-solved", "reference-values", "extra-node-count", "error", "new-node-count"}
            ]
            && BooleanQ[x["is-solved"]]
            && VectorQ[x["reference-values"], positiveRealQ] && Length@x["reference-values"] > 0
            && IntegerQ[x["extra-node-count"]] && x["extra-node-count"] == Length@x["reference-values"] - 1
            && MatchQ[x["error"], _?realQ|Infinity]
            && IntegerQ[x["new-node-count"]] && x["new-node-count"] >= x["extra-node-count"]
        ];
    validSolutionStateQ = 
        Function[
            {x},
            AssociationQ[x]
            && And@@Map[KeyExistsQ[x, #]&, {"solution-state", "approximation-state"}]
            && validSolutionStateStructureQ[x["solution-state"]]
            && validApproximationStateQ[x["approximation-state"]]
        ];
    loadSolutionStatePure =
        Function[
            {sol},
            solutionState = sol["solution-state"];
            loadApproximationState[sol["approximation-state"]];
            (*do not perform any tests here because validApproximationStateQ
            already ensures that the state is valid*)
            {u0, lambda, useCorrections} = getApproximationParamaters[];
            (*use the parameters inside the approximation instead of passing them separately*)
            True
        ];
    loadSolutionState[sol_?validSolutionStateQ] :=
        loadSolutionStatePure[sol];
    importSolutionState[fileName_String] :=
        Block[
            {obj},
            obj = importObject[fileName];
            Catch[
                If[
                    FailureQ[obj] || !validSolutionStateQ[obj["object"]],
                    messagePrint[
                        "ERR",
                        "importApproximationState:",
                        "Failed to interpret the file as a valid approximation state."
                    ]; Throw[False]
                ];
                loadSolutionStatePure[obj["object"]](*this one always returns True*)
            ]
        ];   


End[];

EndPackage[]