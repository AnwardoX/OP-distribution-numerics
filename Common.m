BeginPackage["OPDistribution`Common`"];

messagePrint::usage = 
    "messagePrint[tag, expr] prints a message 'expr' with corresponding to the specified tag.
    Standard tags: {'ERR', 'WRN', 'INF', 'DBG'}, in the order of their priority.
    If the tag is not a standard one, its priority is the lowest. 
    Messages with priority below the current verobisty level are not printed. See 'setMessageVerbosity'.
    All arguments are evaluated only at the moment of printing.";
setMessageVerbosity::usage =
    "setMessageVerbosity[lvl] sets the current level of message printing to 'lvl'.
    All messages with priority level above lvl are not printed.";
quietCheck::usage = 
    "";

realQ::usage = 
    "Test on reality.";
positiveRealQ::usage = 
    "Test on positive real number.";
nonnegativeRealQ::usage = 
    "Test on nonnegative real number.";
nonnegativeRealVectorQ::usage = 
    "Test on vector of nonnegative real numbers.";
positiveIntegerQ::usage = 
    "Test on positive integer.";
nonnegativeIntegerQ::usage =  
    "Test on nonnegative interger.";
positiveRealVectorQ::usage = 
    "Test on real vector with all components positive.";
realVectorQ::usage = 
    "Test on real vector.";
complexVectorQ::usage = 
    "Test on complex vector.";
complexMatrixQ::usage = 
    "Test on complex matrix.";
realMatrixQ::usage = 
    "Test on real matrix.";

phi12::usage = 
	"A pure function for the Fourier of y Log[1/y].
    Obtained as continuation of Laplace transform to imaginary value: Exp[-a] -> Exp[-I s]";

numericValues::usage = 
    "numericValues[symbol, n] returns a list of downvalues pairs arg -> val, 
    where arg is a list of up to n numeric arguments, and val is a numeric value.
    A downvalue is a direct assigment of the form f[arg1, arg2, ...] = val.
    ";
replaceNumericValues::usage = 
    "replaceNumericValues[symbol, n, vals] deletes all downvalues that would be returned by numericValues[symbol, n] and replaces them with val.
    The 'vals' argument is tested to be a valid list of {arg1, arg2, ...} -> Hold[value], where all args and value are Numeric.";
validNumericValuesTest::usage = 
    "validNumericValuesTest[num] returns a pure function that tests whether argument is valid list of
    pairs {arg} -> Hold[val], where arg is a list of numeric values, and val is a numerical values.
    ";
exportObject::usage = 
    "exportObject[obj, fileName] attempts to save the specified object to a file.
    Returns True on success, otherwise returns False or unevaluated.";
importObject::usage = 
    "importObject[fileName] attempts to read the specified file as a ByteArray created by exportObject.
    Returns $Failed on Failure, otherwise returns an Associations with a guaranteed field 'object', containing the exported object.
    ";

Begin["`Private`"];

    standardTags = 
        {"ERR", "WRN", "INF", "DBG"};(*in order of priority*)
    validVerbosityQ = Function[{lvl}, IntegerQ[lvl] && 1 <= lvl <= Length@standardTags + 1];
    verbosityLevel = 1;
    setMessageVerbosity[lvl_?validVerbosityQ] := 
        (
            If[lvl > 3, Off[General::stop], On[General::stop]];
            verbosityLevel = lvl;
            True
        );
    tagPriority[tag_?StringQ] :=
        Block[
            {pos = Position[standardTags, tag]},
            If[
                Length@pos == 0,
                Length@standardTags + 1,
                pos[[1, 1]]
            ]
        ];
    messagePrint[tag_?StringQ, msg___] := 
        If[
            tagPriority[tag] <= verbosityLevel,
            Print[Column@{tag, Evaluate@msg, "\n"}]
            (*Column and Row are needed to circumvent a bug in wolfram text ouput:
            Print["a\n", "b"] -> "ab" // WTF?
            Print[Column@{"a", "b"}] -> "a\nb" // OK
            Probabbly, it uses Row internally, and in wolfram commnad line Row@{"a\n", "b"} gives ab
            *)
        ];
    SetAttributes[messagePrint, HoldRest];

    quietCheck[expr_, failExpr_, messages___] :=
        Quiet[
            Check[expr, failExpr, messages],
            messages
        ];
    SetAttributes[quietCheck, HoldAllComplete];

	realQ = Function[{x}, NumericQ[x] && Im[x] == 0];
    positiveRealQ = Function[{x}, realQ[x] && x > 0];
	nonnegativeRealQ = Function[{x}, realQ[x] && x >= 0];
	nonnegativeRealVectorQ = Function[{x}, VectorQ[x, nonnegativeRealQ]];
    positiveIntegerQ = Function[{x}, IntegerQ[x] && x > 0];
    nonnegativeIntegerQ = Function[{x}, IntegerQ[x] && x >= 0];
    complexVectorQ = Function[{x}, VectorQ[x, NumericQ]];
    positiveRealVectorQ = Function[{x}, VectorQ[x, positiveRealQ]];
    realVectorQ = Function[{x}, VectorQ[x, realQ]];
    complexMatrixQ = Function[{x}, MatrixQ[x, NumericQ]];
    realMatrixQ = Function[{x}, MatrixQ[x, realQ]];

    phi12 = Function[{s}, (Log[I s] + EulerGamma - 1) / (I s)^2];

    Unprotect[extractPatternedValues, deletePatternedValues];
    extractPatternedValues[symbol_Symbol, argTest_, valTest_] :=
        Block[
            {downvalues = DownValues[symbol]},
            Cases[
                downvalues, 
                HoldPattern[head1_[head2_[arg___]] :> val_] /; 
                head1 === HoldPattern && head2 === symbol
                && argTest[{arg}] && valTest[val] :> {arg} -> Hold[val]
            ]
        ];
    SetAttributes[extractPatternedValues, HoldAllComplete];
    deletePatternedValues[symbol_Symbol, argTest_, valTest_] :=
        Block[
            {downvalues = DownValues[symbol]},
            DownValues[symbol] = 
                Cases[
                    downvalues, 
                    Except[
                        HoldPattern[head1_[head2_[arg___]] :> val_] /; 
                        head1 === HoldPattern && head2 === symbol
                        && argTest[{arg}] && valTest[val]
                    ]
                ];
        ];
    SetAttributes[deletePatternedValues, HoldAllComplete];
    Protect[extractPatternedValues, deletePatternedValues];
    sequenceOfNumericQ = 
        Function[
            {upToNum},
            Function[{x}, 1 <= Length@x <= upToNum && VectorQ[x, NumericQ]]
        ];
    numericValues[symbol_Symbol, argCount_?positiveIntegerQ] :=
        Block[
            {argTest = sequenceOfNumericQ[argCount]},
            extractPatternedValues[symbol, argTest, NumericQ]
        ];
    SetAttributes[numericValues, HoldAllComplete];
    validNumericPairQ = 
        Function[
            {upToNum},
            Function[{x}, MatchQ[x, (arg_?(sequenceOfNumericQ[upToNum]) -> Hold[val_]) /; NumericQ[val]]]
        ];
    validNumericValuesQ = 
        Function[
            {upToNum},
            Function[{x}, ListQ[x] && Length@x >= 0 && VectorQ[x, validNumericPairQ[upToNum]]]
        ];
    replaceNumericValues[
        symbol_Symbol, argCount_?positiveIntegerQ, values_
        ] /; validNumericValuesQ[argCount][values] :=
        Block[
            {argTest = sequenceOfNumericQ[argCount]},
            deletePatternedValues[symbol, argTest, NumericQ];
            AppendTo[
                DownValues[symbol],
                values /. (((a_) -> Hold[b_]) :> ((HoldPattern[a] //. List :> symbol) :> b))
            ];
        ];
    SetAttributes[replaceNumericValues, HoldAllComplete];
    validNumericValuesTest[n_?positiveIntegerQ] := validNumericValuesQ[n];

    exportObject[obj_, filePath_String] := 
        Block[
            {file, b, status},
            Catch[
                file = OpenWrite[filePath, BinaryFormat -> True];
                If[
                    FailureQ[file],
                    messagePrint[
                        "ERR",
                        "exportObject:",
                        "Failed to open the specified file: ",
                        filePath
                    ]; Throw[False]
                ];
                b = BinarySerialize[Association["object" -> obj, "date" -> Date[]]];
                status = BinaryWrite[file, b];
                Close[file]; 
                If[
                    FailureQ[status],
                    messagePrint[
                        "ERR",
                        "exportObject:",
                        "Failed to write in the specified file: ",
                        filePath
                    ]; Throw[False]
                ];
                True
            ]
        ];
    importObject[filePath_String] :=
        Block[
            {file, b},
            Catch[
                file = OpenRead[filePath, BinaryFormat -> True];
                If[
                    FailureQ[file],
                    messagePrint[
                        "ERR",
                        "importObject:",
                        "Failed to open the specified file: ",
                        filePath
                    ]; Throw[$Failed]
                ];
                b = BinaryDeserialize@ReadByteArray[file];
                Close[file];   
                If[
                    !AssociationQ[b] || !KeyExistsQ[b,"object"],
                    b = $Failed
                ];
                If[
                    FailureQ[b],
                    messagePrint[
                        "ERR",
                        "importObject:",
                        "Failed to read the specified file: ",
                        filePath
                    ]; Throw[$Failed]
                ];    
                b      
            ]
        ];

End[];

EndPackage[]