BeginPackage["OPDistribution`mFunction`"];

m::usage = 
    "m[sigma, w] returns the current approximation for m[sigma, w] for a complex sigma and w in [0, 1].";
dM::usage = 
    "dM[sigma, w] returns the current approximation for D[m[sigma, w], w] for a complex sigma and w in [0, 1].";
pM::usage = 
    "pM[sigma, w] returns the current approximation for - I D[m[sigma, w], sigma] for a complex sigma and w in [0, 1].";
p2M::usage = 
    "p2M[sigma, w] returns the current approximation for - I D[pM[sigma, w], sigma] for a complex sigma and w in [0, 1].";
m1::usage = 
    "m1[w] returns the current approximation for m1[w] for w in [0, 1].";
dM1::usage = 
    "dM1[w] returns the current approximation for D[m1[w], w] for w in [0, 1].";
j::usage = 
    "j[s] returns the current approximation of the J integral for any complex s.";
dJ::usage = 
    "dJ[s] returns the current approximation for -I D[J[s], s] for any complex s.";
d2J::usage = 
    "dJ[s] returns the current approximation for -I D[dJ[s], s] for any complex s.";
f::usage = 
    "f[w0] returns the value of the special function F[w0].";
validWQ::usage = 
    "validWQ[x] tests whether x is a real number in [0, 1].";

setApproximationParameters::usage = 
    "setApproximationParameters[u0, lambda] sets the value of model constants u0, lambda.
    setApproximationParameters[u0, lambda, b] additionally sets the value of the 'includeCorrections' flag to b.
    The approximation is then reset to a default value (all cached values are deleted).
    The function retunrs True on success. Otherwise, returns unevaluated.";
getApproximationParamaters::usage = 
    "Returns the list {u0, lambda, includeCorrections} of the current values of the model parameters.";

getExtraNodes::usage = 
    "getExtraNodes[] returns the list of nodes used for the current approximation.
    getExtraNodes[nc] returns the list of nodes that will be used to build an approximation on nc nodes.";
updateReferenceValues::usage =
    "updateReferenceValues[extraValues] updates the reference values
    of r1 extra values and rebuilds the approximation appropriately.
    The length of the 'extraValues' list controls the number of nodes used to update the approximation.
    If the 'includeCorrections' flag is set to True, then the values are used to update the correction.
    In the converse situation, i. e. when 'includeCorrections' == False, the list is discraded.
    updateReferenceValues[rZero, extraValues] works as updateReferenceValues[extraValues], but also updates the reference value of m1[0].
    ";
resetApproximation::usage = 
    "resetApproximation[] resets the whole approximation to it's default state directly after initialization.
    The function retunrs True on success. Otherwise, returns unevaluated.
    ";

collectApproximationState::usage = 
    "collectApproximationState returns the structure of the current state suitable for saving and loading.
    Accepts a single mandatory Boolean argument that controls whether cached values of various functions are also to be saved.
    ";
exportApproximationState::usage = 
    "exportApproximationState[fileName, saveStorage] collects the current state of the approximation by
    collectApproximationState[saveStorage] and saves it to a binary file 'fileName'.
    Returns True on sucess. Otherwise, returns False or remains unevaluated.
    ";
loadApproximationState::usage = 
    "loadApproximationState[state] replaces the current state of approximation with the one provided.
    The approximation is reset, so that all cached values of special functions are lost.
    Returns True on success, otherwise returns unevaluated.";
importApproximationState::usage = 
    "importApproximationState[fileName] replaces the current state of the approximation with the one stored in the file 'fileName'.
    The approximation is reset, so that all cached values of special functions are lost.
    Returns True on sucess. Otherwise, returns False or remains unevaluated.";
validApproximationStateQ::usage = 
    "validApproximationStateQ[state] tests whether the argument is a valid structure of the Approximation State.";

Begin["`Private`"];

    Needs["OPDistribution`Common`", "Common.m"];
    includeCorrections = True;

    (*value type control*)
    validWQ = Function[{x}, nonnegativeRealQ[x] && x <= 1];

    (*code to circumvent problems in finite precision arithmetics*)
    careful[n_?nonnegativeIntegerQ, s_?NumericQ, func_Function]:=
        If[
            Im[s] < 0,
            func[n, s],
            Block[
                {se = SetPrecision[s, Round@Im[s] + 40]},
                func[n, se]
            ]
        ];
    careful[s_?NumericQ, func_Function]:=
        If[
            Im[s] < 0,
            func[s],
            Block[
                {se = SetPrecision[s, Round@Im[s] + 40]},
                func[se]
            ]
        ];    
    (*explicit expressions for some special functions*)
    (*phi0*)
    phi0Pure = 
        Function[
            {s}, 
            (*
            FullSimplify[Integrate[(Exp[a w] - 1 - a w)/(w^2 Sqrt[1 - w^2]), {w, 0, 1}, 
            Assumptions -> a > 0]] /. a -> I s
            *)
            -(1/12) s^2 (
                3 \[Pi] HypergeometricPFQ[{1/2}, {3/2, 2}, -(s^2/4)]
                 + 2 I s HypergeometricPFQ[{1, 1}, {3/2, 2, 5/2}, -(s^2/4)]
            )
        ];
    phi0[s_?NumericQ] := phi0[s] = careful[s, phi0Pure];
    dPhi0Pure = 
        Function[
            {s}, 
            (*FullSimplify[-I D[phi0Pure[s], s] ]*)
            1/12 I (
                3 \[Pi] s HypergeometricPFQ[{1/2}, {3/2, 2}, -(s^2/4)]
                 + 2 I s^2 HypergeometricPFQ[{1, 1}, {3/2, 2, 5/2}, -(s^2/4)] 
                 + 6 \[Pi] (BesselJ[1, s] + I StruveH[1, s])
            )
        ];
    dPhi0[s_?NumericQ] := dPhi0[s] = careful[s, dPhi0Pure];
    d2Phi0Pure = 
        Function[
            {s}, 
            (*FullSimplify[-I D[dPhi0Pure[s], s] ]*)
            1/2 \[Pi] (BesselJ[0, s] + I StruveH[0, s])
        ];
    d2Phi0[s_?NumericQ] := d2Phi0[s] = careful[s, d2Phi0Pure];
    (*i*)
    iPure = 
        Function[
            {k, s}, (*
            Integrate[Sqrt[1 - w^2] w^k Exp[a w], {w, 0, 1}, 
            Assumptions -> {a > 0, k \[Element] Integers, k >= 0}] /. a -> I s*)
            1/8 \[Pi] (2 Gamma[(1 + k)/2] 
             * HypergeometricPFQRegularized[{(1 + k)/2}, {1/2, (4 + k)/2}, -(s^2/4)] 
             + I s Gamma[1 + k/2] 
             * HypergeometricPFQRegularized[{(2 + k)/2}, {3/2, (5 + k)/2}, -(s^2/4)]
            )
        ];
    i[n_?nonnegativeIntegerQ, sigma_?NumericQ] := i[n, sigma] = 
        careful[n, sigma, iPure];
    (*phi1 — can be expressed via i[n, s]*)
    phi1[s_?NumericQ] := phi1[s] = I s (i[0, s] - i[0, 0]);
    dPhi1[s_?NumericQ] := dPhi1[s] = i[0, s] - i[0, 0] + I s i[1, s];
    d2Phi1[s_?NumericQ] := d2Phi1[s] = 2 i[1, s] + I s i[2, s];
    (*G*)
    g0Pure = 
        Function[
            {w0},
            (*Derived expression ,equivalent to:
            NIntegrate[ - Sqrt[1 - w^2] * ((w + w0) Log[w + w0] - w0 Log[w0]) / w, {w, 0, 1}]*)
             - (1 + Log[w0]) Pi/4
             + 1/8 Re[
             -(4 + \[Pi]^2) w0 + 2 \[Pi] w0^2
             + 4 w0 ArcSec[w0]^2 + \[Pi] (3 + Log[4] + 2 Log[w0])
             + 4 ArcSec[w0] * (
                I \[Pi] - w0 Sqrt[-1 + w0^2]
                 - Log[I - w0 - Sqrt[-1 + w0^2]]
                 + Log[I + w0 - Sqrt[-1 + w0^2]])
             + 4 I PolyLog[2, (I - Sqrt[-1 + w0^2])/w0] 
             - 4 I PolyLog[2, (-I + Sqrt[-1 + w0^2])/w0]
            ]
        ];
    gPure = 
        Function[
            {w0, s},
             - NIntegrate[
                Sqrt[1 - w^2] Exp[I s w] * ((w + w0) Log[w + w0] - w0 Log[w0]) / w,
                {w, 0, 1}
            ]
        ];
    dGPure = 
        Function[
            {w0, s},
             - NIntegrate[
                Sqrt[1 - w^2] Exp[I s w] * ((w + w0) Log[w + w0] - w0 Log[w0]),
                {w, 0, 1}
            ]
        ];
    d2GPure = 
        Function[
            {w0, s},
             - NIntegrate[
                Sqrt[1 - w^2] Exp[I s w] * w * ((w + w0) Log[w + w0] - w0 Log[w0]),
                {w, 0, 1}
            ]
        ];
    g[w0_?NumericQ, 0] := g[w0, 0] = g0Pure[w0];
    g[w0_?NumericQ, 0.] := g[w0, 0.] = g0Pure[w0];
    (*no need to use careful[] here, because the integration is numerical and thus stable*)
    g[w0_?NumericQ, s_?NumericQ] := g[w0, s] = gPure[w0, s];
    dG[w0_?NumericQ, s_?NumericQ] := dG[w0, s] = dGPure[w0, s];
    d2G[w0_?NumericQ, s_?NumericQ] := d2G[w0, s] = d2GPure[w0, s];
    (*f*)
    fPure = 
        Function[
            {w0}, 
            (*NIntegrate[
            Sqrt[1 - w1^2] ((w0 + w1) Log[1/(w0 + w1)] - w0 Log[1/w0])/w1 - 
            1/Sqrt[1 - w1^2] ((w1 + w0) Log[w1/w0 + 1] - w1)/
            w1^2 (w0 + w1^3), {w1, 0, 1}]*)
             - Pi/4 (Log[w0] + 1) + 
             - (
                w0 - w0 ArcCsc[w0]^2 + 
                1/4 \[Pi] (-2 + I \[Pi] + 2 w0 (-w0 + Sqrt[-1 + w0^2]) - Log[4]) + 
                1/2 \[Pi] Log[(w0 + Sqrt[-1 + w0^2])/w0] + 
                ArcCsc[w0] (\[Pi] (-I + w0) - w0 Sqrt[-1 + w0^2] + 
                Log[I - w0 + Sqrt[-1 + w0^2]] - Log[I + w0 + Sqrt[-1 + w0^2]]) + 
                I PolyLog[2, -((I + Sqrt[-1 + w0^2])/w0)] - 
                I PolyLog[2, (I + Sqrt[-1 + w0^2])/w0]
            )
        ];
    f[w0_?NumericQ] := f[w0] = fPure[w0];

    (*calculation of m function*)
    (*singular part*)
    m1LeadingPure = 
        Function[{w0, w}, u0 (w0 + w)];
    m1SingularPure = 
        Function[{w0, w}, -lambda * u0 * (Log[u0] w + (w + w0) Log[w + w0] - w0 Log[w0])];
    dM1LeadingPure = 
        Function[{w0, w}, u0];
    dM1SingularPure = 
        Function[{w0, w}, -lambda * u0 * (Log[u0] + 1 + Log[w + w0])];
    (*analytical part*)
    m1AnalyticalPure = 
        Function[{w0, w}, lambda * u0 * (coefficients.(w^Range[1, extraNodeCount]))];
        (*well defined even for extraNodeCount = 0, coefficients = {}*)
    dM1AnalyticalPure = 
        Function[
            {w0, w}, (*well defined even for extraNodeCount = 0, coefficients = {}*)
            If[
                extraNodeCount > 0,
                First[coefficients]
                 + Rest[coefficients].(Range[2, extraNodeCount] * (w^Range[extraNodeCount - 1])),
                0
            ] * lambda * u0
        ];
    
    jSingularPure = 
        Function[
            {w0, s},
            lambda ( - Log[u0] * i[0, s] + g[w0, s])
        ];
    dJSingularPure = 
        Function[(*-I D[i[k,s], s] == i[k + 1, s]*)
            {w0, s},
            lambda ( - Log[u0] * i[1, s] + dG[w0, s])
        ];
    d2JSingularPure = 
        Function[
            {w0, s},
            lambda ( - Log[u0] * i[2, s] + d2G[w0, s])
        ];
    jAnalyticalPure = 
        Function[
            {w0, s},
            lambda * (coefficients.Thread[i[Range[0, extraNodeCount - 1], s]])
        ];
    dJAnalyticalPure = 
        Function[
            {w0, s},(*-I D[i[k,s], s] == i[k + 1, s]*)
            lambda * (coefficients.Thread[i[Range[1, extraNodeCount], s]])
        ];
    d2JAnalyticalPure = 
        Function[
            {w0, s},(*-I D[i[k,s], s] == i[k + 1, s]*)
            lambda * (coefficients.Thread[i[Range[2, extraNodeCount + 1], s]])
        ];


    resetStorage[] :=
        (
            Clear[m1, dM1, j, dJ, d2J];
            If[
                includeCorrections,
                (*exact expressions*)
                m1[w_?validWQ] := 
                    m1LeadingPure[W0, w] + m1SingularPure[W0, w] + m1AnalyticalPure[W0, w];
                dM1[w_?validWQ] := 
                    dM1LeadingPure[W0, w] + dM1SingularPure[W0, w] + dM1AnalyticalPure[W0, w];
                j[s_?NumericQ] := j[s] = 
                    jSingularPure[W0, s] + jAnalyticalPure[W0, s];
                dJ[s_?NumericQ] := dJ[s] = 
                    dJSingularPure[W0, s] + dJAnalyticalPure[W0, s];
                d2J[s_?NumericQ] := d2J[s] = 
                    d2JSingularPure[W0, s] + d2JAnalyticalPure[W0, s];,
                (*bare small v approximation*)
                m1[w_?validWQ] := m1LeadingPure[W0, w] + m1SingularPure[W0, w];
                dM1[w_?validWQ] := dM1LeadingPure[W0, w] + dM1SingularPure[W0, w];
                j[s_?NumericQ] = jSingularPure[W0, 0]; (*this reproduces the fact that we only need j[0]*)
                dJ[s_?NumericQ] = 0;
                d2J[s_?NumericQ] = 0;
            ];

            Clear[m, dM, pM, p2M];
            (*the expression for these two automatically inherit the correct structure from r1, J*)
            m[s_?NumericQ, W_?validWQ] := m[s, W] =
                I s m1[W] / u0 + lambda ((W0 + W) phi0[s] + phi1[s] + I s (j[s] - j[0]));
            dM[s_?NumericQ, W_?validWQ] := dM[s, W] = 
                I s dM1[W] / u0 + lambda * phi0[s];
            pM[s_?NumericQ, W_?validWQ] := pM[s, W] = 
                m1[W] / u0 + lambda ((W0 + W) dPhi0[s] + dPhi1[s] + j[s] - j[0] + I s dJ[s]);
            p2M[s_?NumericQ, W_?validWQ] := p2M[s, W] =
                lambda ((W0 + W) d2Phi0[s] + d2Phi1[s] + 2 dJ[s] + I s d2J[s]);
        );
    resetApproximation[] :=
        (
            (*some initialization for the behavior of r1*)
            W0 = Pi/4 / ProductLog[Pi/4 u0];
            extraNodeCount = 0;
            coefficients = {};(*Length@coefficients = extraNodeCount, always*)
            resetStorage[]; (*clear all public values as well*)
            True
        );

    setApproximationParameters[uN_?positiveRealQ, l_?positiveRealQ, st_?BooleanQ] :=
        (
            {u0, lambda} = {uN, l};
            includeCorrections = st;
            resetApproximation[]; (*resetting the whole approximation, not just the cache of r function*)
            True
        );
    setApproximationParameters[uN_?positiveRealQ, l_?positiveRealQ] :=
        setApproximationParameters[uN, l, includeCorrections];
    getApproximationParamaters[] := {u0, lambda, includeCorrections};

    (*the properly ordered list of nodes that is used for updateReferenceValues*)
    getExtraNodes[nc_?positiveIntegerQ] := N[(1 + Cos[Pi (2 Range[nc] - 1) / (2 nc)]) / 2];
    getExtraNodes[0] := {};
    getExtraNodes[] := getExtraNodes[extraNodeCount];

    getMatrix[nc_?positiveIntegerQ] := 
        Array[
            N[((1 + Cos[(2 #1 - 1)/(2 nc) Pi])/2)^#2] &,
            {nc,  nc}
        ];
    updateReferenceValues[extraValues_?positiveRealVectorQ] :=
        Block[
            {matr, vect},
            If[
                includeCorrections,
                extraNodeCount = Length@extraValues;
            ];
            coefficients = 
                If[
                    includeCorrections && Length@extraValues > 0,
                    matr = getMatrix[extraNodeCount];
                    vect = 
                    (
                        extraValues
                        - Thread[m1LeadingPure[W0, getExtraNodes[]]]
                        - Thread[m1SingularPure[W0, getExtraNodes[]]]
                    ) / (lambda u0);
                    LinearSolve[matr, vect],
                    {}
                ];
            resetStorage[];(*clearing memorized values*)
            True            
        ];
    updateReferenceValues[
        valAtZero_?positiveRealQ, 
        extraValues_?positiveRealVectorQ
    ] :=
        (
            W0 = valAtZero / u0;
            updateReferenceValues[extraValues]
        );

    (*init with something*)
    setApproximationParameters[0.2, 0.2];

    (*export/import logics*)
    Unprotect[storageStructureVersion];
    storageStructureVersion = 3;
    (*this is used to make sure that the format is compatible in the future;
    to be changed every time the format of the saved functions changes*)
    Protect[storageStructureVersion];

    cachedSymbols = 
        Transpose@{
            {phi0, 1}, {dPhi0, 1}, {d2Phi0, 1}, (*no need to cache Phi1*)
            {i, 2}, {g, 2}, {dG, 2}, {d2G, 2}, {f, 1},
            {m, 2}, {dM, 2}, {pM, 2}, {p2M, 2} (*no need to memorize J*)
        };
    collectApproximationState[saveStorage_?BooleanQ] :=
        Block[
            {storage = {}},
            If[
                saveStorage,
                storage = Association[
                    "version" -> storageStructureVersion,
                    "cache" ->
                        MapThread[
                            numericValues[#1, #2]&,
                            cachedSymbols
                        ]
                ]
            ];
            Association[
                "parameters" -> getApproximationParamaters[],
                "coefficients" -> {W0, coefficients},
                "storage" -> storage
            ]      
        ];
    exportApproximationState[fileName_String, saveStorage_?BooleanQ] :=
        exportObject[collectApproximationState[saveStorage], fileName];

    validApproximationParametersQ = 
        Function[
            {x}, 
            ListQ[x] && Length@x == 3 && 
            (*u0*) positiveRealQ[x[[1]]] 
            && (*lambda*) positiveRealQ[x[[2]]]
            && (*includeCorrections*) BooleanQ[x[[3]]]
        ];
    validCoefficientsQ = 
        Function[
            {x},
            ListQ[x] && Length@x == 2
            && positiveRealQ[x[[1]]] && VectorQ[x[[2]], realQ]
        ];
    validStorageQ = 
        Function[
            {x},
            x === {} ||
            (
                AssociationQ[x]
                && And@@Map[KeyExistsQ[x, #]&, {"version", "cache"}]
                && x["version"] === storageStructureVersion
                && ListQ[x["cache"]]
                && Length[x["cache"]] == Length@First@cachedSymbols
                && And@@MapThread[
                    validNumericValuesTest[#1][#2]&,
                    {Last@cachedSymbols, x["cache"]}
                ]
            )
        ];
    validApproximationStateQ = 
        Function[
            {x},
            AssociationQ@x &&
            And@@Map[KeyExistsQ[x, #]&, {"parameters", "coefficients", "storage"}]
            && validApproximationParametersQ[x["parameters"]]
            && validCoefficientsQ[x["coefficients"]] && validStorageQ[x["storage"]]
        ];
    (*load the storage*)
    loadApproximationStatePure = 
        Function[
            {state},
            (*consistent with getApproximationParameters[]*)
            {u0, lambda, includeCorrections} = state["parameters"];
            {W0, coefficients} = state["coefficients"];
            extraNodeCount = Length@coefficients;
            resetStorage[];
            If[
                Length@state["storage"] > 0,
                MapThread[
                    replaceNumericValues[#1, #2, #3]&,
                    Append[cachedSymbols, state["storage", "cache"]]
                ]
            ];
            True
        ];
    loadApproximationState[state_?validApproximationStateQ] := 
        loadApproximationStatePure[state];
    importApproximationState[fileName_String]:=
        Block[
            {obj},
            Catch[
                obj = importObject[fileName];
                If[
                    FailureQ[obj] || !validApproximationStateQ[obj["object"]],
                    messagePrint[
                        "ERR",
                        "importApproximationState:",
                        "Failed to interpret the file as a valid approximation state."
                    ]; Throw[False]
                ]; 
                loadApproximationStatePure[obj["object"]]           
            ]
        ];

End[];

EndPackage[];