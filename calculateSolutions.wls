#!/usr/bin/env wolframscript

(*a workaround for intel OpenMP bug
see  https://github.com/ContinuumIO/anaconda-issues/issues/11294
appears when trying to import xml and using Parallel Kernels after that
*)
(*SetEnvironment["KMP_INIT_AT_FORK"->"FALSE"];  still does not help somehow...*)
If[
    GetEnvironment["KMP_INIT_AT_FORK"] =!= ("KMP_INIT_AT_FORK"->"FALSE"),
    Print["WRN: env. variable 'KMP_INIT_AT_FORK' is not set.\n
	\tParallel Kernels might not function properly.\n
	\tTry using 'KMP_INIT_AT_FORK=FALSE <command>' to launch this script."];
];

(*Needed to work around ugly Print from parallel kernels*)
SetOptions[$Output, FormatType->OutputForm];

Needs["OPDistribution`Common`", "Common.m"];
Needs["OPDistribution`Probabilities`", "Probabilities.m"];

(*try to read the verbosity setting*)
verbosity = 
	Which[
		Length@$ScriptCommandLine == 2, 3(*default*),
		Length@$ScriptCommandLine == 3,	ToExpression@$ScriptCommandLine[[3]],
		True, messagePrint["ERR", "Invalid command line. Quitting."]; Quit[]; 1
	];
(*try to set verbosity*)
If[
	Not[setMessageVerbosity[verbosity] === True],
	messagePrint["ERR", "Invalid message verbosity argument. Quitting."];
	Quit[];
];
(*check existence and if exists -- reset working directory*)
configPath = $ScriptCommandLine[[2]];
If[
    FileExistsQ[configPath],
    If[
        DirectoryName[configPath] =!= "",
        SetDirectory[DirectoryName[configPath]];
    ];
    messagePrint["INF", "Working directory: ", Directory[]];,
    messagePrint["ERR", "Configuration file not found. Quitting."];
    Quit[];
];
(*Importing config*)
{taskList, kernelCount, filenamePreambule} = 
	Block[
		{config = Association@Import[configPath]},
		{
			Association/@(config["task-list"]),
			config["kernel-count"] /. "Automatic" -> 0,
			config["output-filename-preambule"]
		}
	];

stateFilename[task_]:= 
	StringJoin[
		filenamePreambule, 
		"_g-", ToString[N[task["g"], 4], FormatType -> CForm],
		"_k-", ToString[task["k"]],
		"_corr-", ToString[task["use-corrections"]],
		".dis"
	];
outputFilename[task_]:= 
	StringJoin[
		filenamePreambule, 
		"_g-", ToString[N[task["g"], 4], FormatType -> CForm],
		"_k-", ToString[task["k"]],
		"_corr-", ToString[task["use-corrections"]],
		".out"
	];

(*launching necessary number of parallel kernels*)
directory = Directory[];
If[
	kernelCount > 0,
	LaunchKernels[kernelCount];
	realKernelCount = Min[$KernelCount, Length@ParallelEvaluate[SetDirectory[directory]]];
	If[
		realKernelCount < kernelCount,
		messagePrint["WRN", "Only "<>ToString@realKernelCount<>" out of "<>ToString@kernelCount<>" kernels were successfully launched."]
	],
	realKernelCount = 0
];
If[
	realKernelCount > 0,
	ParallelEvaluate@Needs["OPDistribution`Probabilities`",  "Probabilities.m"];
	ParallelEvaluate@Needs["OPDistribution`Common`", "Common.m"];
	ParallelEvaluate[setMessageVerbosity[verbosity]];
];
map = If[realKernelCount > 0, ParallelMap, Map];

empiricalPlotPoints[u0_?positiveRealQ, g_?positiveRealQ, nu0_?positiveRealQ] :=
	Block[
		{min, max, var, v},
		var = Pi/2 * 2 nu0 * g u0;
		v = g / u0;
		{min, max, step} = 
			Which[
				var < 1/9,
				{1 - 4 Sqrt[var], 1 + 4 Sqrt[var], 8 Sqrt[var] / 100},
				True,
				{step, 1.1 * 2.43474 / Sqrt[v], 0.05}
			];
		Range[min, max, step]
	];
(*empirical data for {v, max}:
{{0.431, 3.5}, {0.193, 4}, {0.116, 6}, {0.017, 12}, {0.00186, 
  60}, {0.00552, 31}}*)

{totalTime, finishedTasksData} = 
	AbsoluteTiming@Flatten@map[
		Block[
			{
				k = #["k"], useCorr = #["use-corrections"], g = #["g"],
				outFile, stateFile, outputData, solveTime, pars, plotTime, plotPoints, out
			},
			outFile = outputFilename[#];
			stateFile = stateFilename[#];
			outputData = Association[];
			status = 
				Catch[
					If[
						FileExistsQ[outFile] || FileExistsQ[stateFile],
						messagePrint["INF", "One of output files ", {outFile, stateFile}, " already exists. Skipping."];
						Throw[False]
					];

					{solveTime, out} = AbsoluteTiming[setParameters[k, g, "Box", useCorr]];
					AppendTo[outputData, "solving-time" -> solveTime];
					If[
						Not[out === True],
						messagePrint["WRN", "Task solution failed. Skipping."];
						Throw[False]
					];

					pars = getParameters[];
					plotPoints = empiricalPlotPoints[pars["u0"], pars["\[Lambda]"], pars["nu-0"]];
					{plotTime, out} = AbsoluteTiming[Map[onsiteMarginalProbability, plotPoints]];
					AppendTo[outputData, "plotting-time" -> plotTime];
					AppendTo[outputData, "plot-points" -> Transpose@{plotPoints, out}];
					If[
						Not@VectorQ[out, positiveRealQ],
						messagePrint["WRN", "Task plotting failed. Skipping."];
						Throw[False]
					];

					out = exportModelState[stateFile, True];
					AppendTo[outputData, "model-state-file" -> stateFile];
					If[
						Not[out === True],
						messagePrint["WRN", "Saving the model state failed."];
						Throw[False]
					];

					out = exportObject[outputData, outFile];
					If[
						Not[out === True],
						messagePrint["WRN", "Saving the output file failed."];
					];
					True
				];
			Association[
				"task" -> #,
				"output-file" -> outFile,
				"state-file" -> stateFile,
				"solving-time" -> outputData["solving-time"],
				"plotting-time" -> outputData["plotting-time"],
				"is-successfull" -> status
			]
		]&,
		taskList
	];

(*print out statistics*)
finishedTasksData = Select[finishedTasksData, (#["is-successfull"]&)]; (*drop incomplete tasks*)
finishedTasksData = GroupBy[finishedTasksData, ({#["task", "k"], #["task", "g"], #["task", "use-corrections"]}&)];
Map[
	Block[
		{pars = #1, data = finishedTasksData[#1]},
		Print["\n"];
		Print["For k = ", pars[[1]], ", g = ", pars[[2]], ", corrections = ", pars[[3]]];
		Print[
			"\tSolving time: ",
			Mean@Flatten[Key["solving-time"]/@data]
		];
		Print[
			"\tPlotting time: ",
			Mean@Flatten[Key["plotting-time"]/@data]
		];
		Print["\tFile names:"];
		Print["\t\t", {Key["output-file"][#], Key["state-file"][#]}]&/@data;
	]&,
	Keys[finishedTasksData]
];
Print["All tasks calculated in ", totalTime];