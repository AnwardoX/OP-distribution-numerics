BeginPackage["OPDistribution`Probabilities`"];

onsiteConditionalProbability::usage = 
    "onsiteConditionalProbability[x, y, w]
    onsiteConditionalProbability[x, y]";
onsiteJointProbability::usage = 
    "onsiteJointProbability[x, y, w]
    onsiteJointProbability[x, y]";
onsiteMarginalProbability::usage = 
    "onsiteMarginalProbability[y, w]
    onsiteMarginalProbability[y]";

setParameters::usage = 
    "setParameters[K, g, dist-type, useCorr] sets the corresponding parameters of the model,
    the type of distribution to be used and  the 'useCorrections' flag of the underlying approximation accordingly.
    Returns True on success. Otherwise, returns False or remains unevaluated.";

getParameters::usage = 
    "getParameters[] returns the current values of physical parameters: K, g, \[Delta], v, \[Kappa]
    and the values of the 'useCorrections' flag.
    ";

collectModelState::usage = 
    "";
exportModelState::usage = 
    "";
loadModelState::usage = 
    "loadModelState[st]
    loadModelState[st, dryRun]";
importModelState::usage = 
    "";

Begin["`Private`"];

    Needs["OPDistribution`Common`", "Common.m"];
    Needs["OPDistribution`mFunction`", "mFunction.m"];
    Needs["OPDistribution`mSolver`", "mSolver.m"];

    (*internal settings*)
    sigmaRadius = 0.1;
    targetPrecision = 5;
    setSolverParameters[sigmaRadius, targetPrecision, Automatic];

    (*x distribution — chosen among implemented candiates*)
    xiDistributionDictionary = Association[
        "Gaussian" -> (Exp[-#^2 / 2] / Sqrt[2 Pi]&),
        "Box" -> (Piecewise@{{1/2, -1 < # < 1}, {0, True}}&)
    ];
    xiDistributionTypes = Keys@xiDistributionDictionary;
    (*model functions of the RHS of the self-consistency equation*)
    validDistributionTypeQ = Function[{x}, MemberQ[xiDistributionTypes, x]];
    omegaPure = 1 / Sqrt[1 + #^2]&; (*for pure BCS at zero temperature*)
    dOmegaPure = - #/(1 + #^2)^(3/2)&; (*D[omegaPure[x], x]*)
    (*mean field Delta*)
    findMeanFieldDelta[
        gVal_?positiveRealQ, KVal_?positiveIntegerQ,
        type_?validDistributionTypeQ] :=
        Block[
            {eq, sol, lambdaVal, s, xiDistr = xiDistributionDictionary[type], mfDelta},
            eq[s_?NumericQ] := 
                (gVal) NIntegrate[
                    omegaPure[x] * xiDistr[2 Exp[s] * x],
                    {x, -Infinity, Infinity}
                    ] - 1;
            lambdaVal = 2 xiDistr[0] gVal;
            sol = FindRoot[eq[s], {s, -1/lambdaVal}];
            If[
                MatchQ[sol, {s -> _?realQ}],
                (
                    mfDelta = 2 Exp[s] /. sol;
                    messagePrint[
                        "INF", "findMeanFieldDelta:",
                        "New value of mean field Delta: ", mfDelta
                    ];
                    mfDelta
                ),
                (
                    messagePrint[
                        "ERR", "findMeanFieldDelta:",
                        "Failed to solve the self-consistency equation."
                    ];
                    $Failed
                )
            ]
        ];

    (*onsite joint probability*)
    resetProbability[] := 
    (
        Clear[onsiteConditionalProbability];
        onsiteConditionalProbability[
            X_?realQ, Y_?nonnegativeRealQ, Optional[W_?validWQ, 0]
        ] := 
            If[
                isInitialized,
                onsiteConditionalProbability[X, Y, W] = 
                    Block[
                        {om = omegaPure[X/Y], dom = - Abs[X] dOmegaPure[X/Y] / Y^2},
                        Abs@sigmaNIntegrate[ (*Abs because Probability always > 0*)
                            (Exp[m[#, om] + I # (W - Y / u0)] * 
                            (
                                1 + 
                                (I u0 / #) (1 - Exp[ + I # Y / u0]) (*choosing Y0 = 0*)
                                * dM[#, om] * dom
                            )&),
                            sigmaRadius, targetPrecision,
                            naturalCutoff[u0, lambda]
                        ] / (2 Pi u0)
                    ],
                $Failed
            ];
        Clear[onsiteJointProbability];
        onsiteJointProbability[
            X_?realQ, Y_?nonnegativeRealQ, Optional[W_?validWQ, 0]
        ] := 
            If[
                isInitialized,
                (meanFieldDelta * xiDistribution[X meanFieldDelta])
                 * onsiteConditionalProbability[X, Y, W],
                $Failed
            ];
        Clear[onsiteMarginalProbability];
        onsiteMarginalProbability[
            Y_?nonnegativeRealQ, Optional[W_?validWQ, 0]
        ] := 
            If[
                isInitialized,
                onsiteMarginalProbability[Y, W] =
                    Abs@sigmaNIntegrate[ (*Abs because Probability always > 0*)
                        (Exp[m[#, 0] + I # (W - Y / u0)]&),
                        sigmaRadius, targetPrecision,
                        naturalCutoff[u0, lambda]
                    ] / (2 Pi u0),
                $Failed
            ];
    );

    (*assemble everything that needs to be done
    every time the parameters of the model are changed*)
    updateModel[
        KVal_?positiveRealQ, gVal_?positiveRealQ,
        dist_?validDistributionTypeQ, useCorr_?BooleanQ
    ] :=
        Block[
            {mfDelta, nuVal, u0Val, distVal, lambdaVal},
            Catch[
                (*step 1: determine the MF delta*)
                mfDelta = findMeanFieldDelta[gVal, KVal, dist];
                If[
                    !positiveRealQ[mfDelta], 
                    messagePrint[
                        "WRN", "updateModel:",
                        "findMeanFieldDelta call failed. Arguments: ",
                        {gVal, KVal, dist}
                    ];
                    Throw[False]
                ];
                (*step 2: try to set the constants*)
                distVal = xiDistributionDictionary[dist];
                nuVal = distVal[0]; 
                lambdaVal = 2 nuVal gVal;
                u0Val = gVal / (KVal * mfDelta); 
                (*yes, g, not lambda, because both g and Delta are dimensionfull*)
                If[
                    setModelParameters[u0Val, lambdaVal, useCorr] =!= True,
                    messagePrint[
                        "WRN", "updateModel:",
                        "mSolver`setModelParameters call failed. Arguments: ",
                        {u0Val, lambdaVal, useCorr}
                    ];
                    Throw[False]
                ];
                (*step 3: try to solve the model*)
                If[
                    updateSolution[]["is-solved"] =!= True,
                    messagePrint[
                        "WRN", "updateModel:",
                        "mSolver`updateSolution call failed."
                    ];
                    Throw[False];
                ];
                (*step 4: clear the cache and update the constants if everything went ok*)
                meanFieldDelta = mfDelta; g = gVal;
                lambda = lambdaVal;
                KNum = KVal; u0 = u0Val; nu0 = nuVal;
                xiDistributionType = dist;
                xiDistribution = distVal;
                useCorrections = useCorr;
                resetProbability[];
                True
            ]
        ];

    (*model constants and distributions*)
    setParameters[
        KVal_?positiveRealQ, gVal_?positiveRealQ, 
        dist_?validDistributionTypeQ|dist:Automatic,
        useCorr_?BooleanQ|useCorr:Automatic
    ] :=
        If[
            (*check if anything has actually changed*)
            !MatchQ[
                {KVal, gVal, dist, useCorr},
                {KNum, g, xiDistributionType|Automatic, useCorrections|Automatic}
            ],
            If[
                updateModel[
                    KVal, gVal, (dist/.Automatic -> xiDistributionType), 
                    (useCorr/.Automatic -> useCorrections)
                ],
                If[!isInitialized, isInitialized = True]; True,
                False
            ],
            True
        ];
    getParameters[] :=
        If[
            isInitialized,
            Association[
                "K" -> KNum, "g" -> g, 
                "\[CapitalDelta]" -> meanFieldDelta, "u0" -> u0,
                "nu-0" -> nu0, "\[Lambda]" -> lambda,
                "distribution" -> xiDistributionType,
                "use-corrections" -> useCorrections,
                "solution-state" -> updateSolution[]
                (*does not actually change anything, just returns the solution state*)
            ],
            Association[]
        ];

    (*init*)
    isInitialized = False;
    xiDistributionType = "Box";
    useCorr = True;
    resetProbability[];

    (*export/import logics*)
    Unprotect[storageStructureVersion];
    storageStructureVersion = 2;
    (*this is used to make sure that the format is compatible in the future;
    to be changed every time the format of the saved functions changes*)
    Protect[storageStructureVersion];

    cachedSymbols = 
        Transpose@{
            {onsiteConditionalProbability, 3},
            {onsiteJointProbability, 3},
            {onsiteMarginalProbability, 2}
        };
    collectModelState[saveStorage_?BooleanQ] :=
        Block[
            {probabilityCache = {}},
            If[
                saveStorage,
                probabilityCache = Association[
                    "version" -> storageStructureVersion,
                    "cache" ->
                        MapThread[
                            numericValues[#1, #2]&,
                            cachedSymbols
                        ]
                ]
            ];
            Association[
                "is-initialized" -> isInitialized,
                "high-energy-parameters" -> If[
                    isInitialized,
                    {
                        meanFieldDelta,
                        g, nu0,
                        xiDistributionType
                    },
                    {}
                ],
                "solution" -> collectSolutionState[saveStorage],
                "probability-cache" -> probabilityCache
            ]
        ];
    exportModelState[fileName_String, saveStorage_?BooleanQ] :=
        exportObject[collectModelState[saveStorage], fileName];
    
    validProbabilityCacheQ = 
        Function[
            {x},
            x === {} ||
            (
                AssociationQ[x] && And@@Map[KeyExistsQ[x, #]&, {"version", "cache"}]
                && x["version"] === storageStructureVersion
                && ListQ[x["cache"]]
                && Length[x["cache"]] == Length@First@cachedSymbols
                && And@@MapThread[
                    validNumericValuesTest[#1][#2]&,
                    {Last@cachedSymbols, x["cache"]}
                ]
            )
        ];
    looksLikeHighEnergyParameterQ = 
        Function[
            {x},
            ListQ[x] && Length@x == 4
            && (*Delta*) positiveRealQ[x[[1]]]
            && (*g*) positiveRealQ[x[[2]]]
            && (*nu*) positiveRealQ[x[[3]]]
            && (*distr*)validDistributionTypeQ[x[[4]]]
        ];
    looksLikeModelStateQ = 
        Function[
            {x},
            AssociationQ[x] &&
            And@@Map[
                KeyExistsQ[x, #]&, 
                {"is-initialized", "high-energy-parameters", "solution", "probability-cache"}
            ]
            && BooleanQ[x["is-initialized"]]
            && If[
                    x["is-initialized"], 
                    looksLikeHighEnergyParameterQ[x["high-energy-parameters"]], 
                    x["high-energy-parameters"] === {}
                ]
            && validSolutionStateQ[x["solution"]]
            && validProbabilityCacheQ[x["probability-cache"]]
        ];
    consistentParametersQ = 
        Function[
            {pars, hep},
            Block[
                {
                    mfDelta, distrType, gVal, nuVal, 
                    KVal, apparentDelta,
                    u0Val, lambdaVal, useCorr
                },
                {mfDelta, gVal, nuVal, distrType} = hep;
                {u0Val, lambdaVal, useCorr} = pars;
                KVal = gVal / (u0Val mfDelta);
                Catch[
                    (*1. K has to be integer*)
                    If[
                        Not[Abs[KVal - Round@KVal] <= 10^(-10)],
                        Throw[False]
                    ];
                    (*2. nu0 and distribution have to be consistent*)
                    If[
                        Not[nuVal == xiDistributionDictionary[distrType][0]],
                        Throw[False]
                    ];
                    (*3. Delta and distribution types have to be consistent*)
                    apparentDelta = findMeanFieldDelta[gVal, Round@KVal, distrType];
                    If[
                        Not[positiveRealQ[apparentDelta] && mfDelta == apparentDelta],
                        Throw[False]
                    ];
                    True
                ]
            ]
        ];
    tryToLoadModelState =
        Block[
            {currentSolutionState, state = #, dryRun = #2},
            Catch[
                (*to be able to revert everything back if needed*)
                currentSolutionState = collectSolutionState[True];
                loadSolutionState[state["solution"]];
                (*load the proposed state to learn the parameters*)
                If[
                    state["is-initialized"],
                    (*if this flag is set, all values have to be consistent*)
                    If[
                        !consistentParametersQ[
                            getApproximationParamaters[],
                            state["high-energy-parameters"]
                        ],
                        messagePrint[
                            "ERR",
                            "loadModelState:",
                            "The parameters of the solution are inconsistent with high-energy parameters.",
                            "High-energy parameters: ", state["high-energy-parameters"],
                            "Solution parameters: ", getApproximationParamaters[]
                        ]; loadSolutionState[currentSolutionState];
                        (*revert the solution state back because it is inconsitent*)
                        Throw[False]
                    ]
                ];
                (*everything is ok — can load the remaining part now*)
                If[
                    !dryRun,
                    isInitialized = state["is-initialized"];
                    If[
                        isInitialized,
                        {meanFieldDelta, g, nu0, xiDistributionType} = state["high-energy-parameters"];
                        {u0, lambda, useCorrections} = getApproximationParamaters[];
                        KNum = Round[g / (u0 meanFieldDelta)];
                        xiDistribution = xiDistributionDictionary[xiDistributionType];
                    ];
                    If[
                        Length@state["probability-cache"] > 0,
                        MapThread[
                            replaceNumericValues[#1, #2, #3]&,
                            Append[cachedSymbols, state["probability-cache", "cache"]]
                        ]
                    ];,
                    loadSolutionState[currentSolutionState];
                ];
                True
            ]
        ]&;
    loadModelState[state_?looksLikeModelStateQ, Optional[dryRun_?BooleanQ, True]] :=
        tryToLoadModelState[state, dryRun];
    importModelState[fileName_String] :=
        Block[
            {obj},
            obj = importObject[fileName];
            Catch[
                If[
                    FailureQ[obj] || !looksLikeModelStateQ[obj["object"]],
                    messagePrint[
                        "ERR",
                        "importApproximationState:",
                        "Failed to interpret the file as a valid approximation state."
                    ]; Throw[False]
                ];
                tryToLoadModelState[obj["object"], False]
            ]
        ]; 
    

End[];

EndPackage[]